import React from 'react';
import './App.css';
import Login from './components/containers/Login';
import Register from './components/containers/Register';
import Dashboard from './components/containers/Dashboard';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import NotFound from './components/containers/NotFound';

function App() {
  return (
    
    <Router>
      <div>
        <Switch>
          <Route exact path="/"><Redirect to="/login"></Redirect></Route>
          <Route path="/login" component={ Login }></Route>
          <Route path="/register" component={ Register }></Route>
          <Route path="/dashboard" component={ Dashboard }></Route>
          <Route path="*" component={ NotFound }></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;

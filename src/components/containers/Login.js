import React from 'react';
import LoginForm from '../forms/LoginForm';
import { Link } from "react-router-dom";

const Login = () => {

  const handleLoginClicked = () => {
    console.log('Triggered from loginForm');
    
  }

    return (
        <div className="container">
          <h2>Login to Survey Puppy</h2>

          <LoginForm click={handleLoginClicked}/><br/>

          <Link to="/register">Create an account. Register now!</Link>
        </div>
      );
    };
    
    export default Login;
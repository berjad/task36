import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => (
  <div className="container">
    {/* <p>The page can not be found!!<span role="img" aria-label="">😨😨😨</span></p> */}
    <img src="https://www.boostability.com/wp-content/uploads/2012/10/BOOST_BLOG_IMAGE_RB_SET_10_404_PAGE_1200x628px_v1_3.jpg" alt="Not found" width="800" /><br/>
    <Link to="/">Go back to Start page</Link>
  </div>
  
);

export default NotFound;
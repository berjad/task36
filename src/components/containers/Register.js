import React from 'react';
import RegisterForm from '../forms/registerForm';
import { Link, useHistory } from "react-router-dom";

const Register = () => {

  const history = useHistory();

  const handleRegisterComplete = (result) => {
    console.log('Triggered from registerForm', result);
      if(result) {
        history.replace("/dashboard");
      }
  };

    return (
        <div className="container">
          <h2>Register for Survey Puppy</h2>

          <RegisterForm complete={handleRegisterComplete}/><br/>

          <Link to="/">I already have an account. Login now!</Link>
        </div>
      );
    };
    
    export default Register;
    

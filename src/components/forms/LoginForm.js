import React from 'react';

const LoginForm = () => {

    return (
     <div className="container">   
      <form>
        <div className="form-group">
          <label>Username: </label>
          <input
          className="form-control"
            type="text"
            placeholder="Enter your username"
          />
        </div>

        <div className="form-group">
          <label>Password: </label>
          <input
          className="form-control"
            type="password"
            placeholder="Enter your password"
          />
        </div>

        <div>
          <button type="button" className="btn btn-primary">
            Login
          </button>
        </div>
      </form>
      </div>
    )

}

export default LoginForm;